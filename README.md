# Basecamp3 Campfire to Matrix bridge

## **This project is not complete! and non functional!**

This bridge currently has broken oauth2, so you cannot login currently. It Does not talk to the Matrix Network whatsoever currently as well.

This is a set of bash scripts to  bridge messages from Basecamp3 Campfire to Matrix. It includes an OAuth script, a Campfire -> Matrix bridge, and a Matrix -> Campfire
appserife.

## Contributing

If you would like to help contribute to the project, you will need some experiene with bash scripting. To find something that you can help with, please take a look at our [1.0.0.0 todo list](https://gitlab.com/minecraftchest1/mx-basecamp3-campfire/-/milestones/1#tab-issues) or at the issues tagged [to-do](https://gitlab.com/minecraftchest1/mx-basecamp3-campfire/-/issues?sort=created_date&state=opened&label_name[]=To-do).


[](url)
