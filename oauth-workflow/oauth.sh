#!/bin/bash
# https://github.com/basecamp/api/blob/master/sections/authentication.md

. env.sh

oauth_client_id="client_id=${1}"
oauth_client_secret="client_secret=${2}"
oauth_redirect_uri="redirect_uri=http://127.0.0.1:60500"

auth_url=
oauth_base="https://launchpad.37signals.com/"
oauth_auth_endpoint="authorization/new"
oauth_token_endpoint="authorization/token"
oauth_type="type=web_server"

oauth_authorize_endpoint="authorization.json"

auth_url+="$oauth_base"
auth_url+="$oauth_auth_endpoint"
auth_url+="?${oauth_type}&${oauth_client_id}&${oauth_redirect_uri}"

token_url=

#====================================================================

debug () {
	echo "$@"
}

echo "Please open the URL in your browser to login."
echo "$auth_url"

libs/web-server.sh oauth

while ! [ -a ${BC3_TMPDIR}/oauth-verify.txt ]
do
	sleep .25
done
verify_token='code='
verify_token+=$(cat ${BC3_TMPDIR}/oauth-verify.txt)

debug $verify_token

## Get access token ##
##==================##
token_url+="$oauth_base"
token_url+="$oauth_token_endpoint"
token_url+="?${oauth_type}&${oauth_client_id}&${oauth_redirect_uri}&${oauth_client_secret}&${verify_token}"

debug "$token_url"

#debug "exiting"
#exit
#debug "exited"

curl -s -X POST "$token_url" > ${BC3_TMPDIR}/authentication.json

oauth_auth_token=`jq '.access_token' ${BC3_TMPDIR}/authentication.json --raw-output`

## Get Projects ##
##==============##

curl -sH "Authorization: Bearer ${oauth_auth_token}" "${oauth_base}${oauth_authorize_endpoint}" > accounts.json

accounts_length=`jq '.accounts | length' accounts.json`
echo "Please select Project/Team"
for ((i=0; i < $accounts_length; i++))
do
	echo -n "  $i. "
	jq ".accounts[$i].name" accounts.json --raw-output
done

read -t 30 -p ' > ' response

# Verify input
re='^[0-9]'
if ! [[ $response =~ $re ]] ; then
	echo "Input not a number" >&2
	exit 1
fi
if [[ $response > $accounts_length ]]
then
	echo "Input out of range" >&2
	exit 1
fi

jq ".accounts[$response].id" accounts.json > token.txt
