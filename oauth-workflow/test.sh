#!/bin/bash
accounts_length=`jq '.accounts | length' accounts.json`


echo "Please select Project/Team"
for ((i=0; i < $accounts_length; i++))
do
    echo -n "  $i. "
    jq ".accounts[$i].name" accounts.json --raw-output
done

read -t 30 -p ' > ' response

# Verify input
re='^[0-9]'
if ! [[ $response =~ $re ]] ; then
    echo "Input not a number" >&2
    exit 1
fi
if [[ $response > $accounts_length ]]
then
    echo "Input out of range" >&2
    exit 1
fi

jq ".accounts[$response].id" accounts.json > token.txt

