#!/bin/bash
. env.sh
case $1 in
    oauth)
        echo 'HTTP/1.1 200 OK'
        echo 'Server: mx-campfire/0.1.0'
        echo "Date: $(date -u '+%a, %d %b %Y %T GMT')"
        echo 'Content-Type: text/html'
        echo 'Content-Length: 34'
        echo 'Connection: close'
        echo 'ETag: "61f9a1d7-2a5"'
        echo 'Accept-Ranges: none'
        echo
        echo 'You can now close this tab/window'

        cat | grep -F 'GET /?code=' | sed 's/GET \/?code=//;s/ HTTP\/1.1//' > ${BC3_TMPDIR}/oauth-verify.txt
        ;;
esac
