#!/bin/bash

source env.sh

case $1 in
    oauth)
        rm ${BC3_TMPDIR}/oauth-verify.txt &> /dev/null
        ncat -p ${BC3_WEBSERVER_PORT} -l -c 'libs/web-response.sh oauth'
        ;;
esac
