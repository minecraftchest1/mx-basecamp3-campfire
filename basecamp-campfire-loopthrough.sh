#!/bin/bash

workdir="~/basecamp"
cd $workdir

campfire-get()
{
	access_token=`jq '.access_token' oauth-workflow/authentication.json --raw-output`
	user_agent="Bc3 Basecamp Matrix Bridge (minecraftchest1+bc3c-mx@outlook.com)"
	api_base="https://3.basecampapi.com/$(cat oauth-workflow/token.txt)"
	auth_header="Authorization: Bearer $access_token"

	#vcurl -sH "$auth_header" -A "$user_agent" $api_base/chats.json > campfire-list.json
	campfire_list_length=`jq '. | length' campfire-list.json`

	echo "Please select Project/Team"
	for ((i=0; i < $campfire_list_length; i++))
	do
		echo -n "  $i. "
		jq ".[$i].bucket.name" campfire-list.json --raw-output
	done
	read -t 30 -p ' > ' response

	# Verify input
	re='^[0-9]'
	if ! [[ $response =~ $re ]] ; then
	echo "Input not a number" >&2
	exit 1
	fi
	if [[ $response > $campfire_list_length ]]
	then
		echo "Input out of range" >&2
		exit 1
	fi

	lines_url=`jq ".[$response].lines_url" --raw-output campfire-list.json`
	curl -sH "$auth_header" -A "$user_agent" $lines_url> campfire-lines	.json

}

campfire-read()
{


	jq '.ide'

	array_length=`jq '. | length' basecamp-campfire-1.json`

	# for ((i = 0; i < $array_length; i++)); do jq ".[$i].id" basecamp-campfire-1.json; done
	#for ((i = 0; i < $array_length; i++))
	#do
	#	jq ".[$i].id" basecamp-campfire-1.json
	#done

	# Find index of latest line we read. #
	# ===================================#
	#
	# Loop through each line in campfire.
	for ((i=0; i < $array_length; i++))
	do
		# see if current line has the latest ID we saved
		if [ $(jq ".[$i].id" basecamp-campfire-1.json) == $(cat basecamp-latest.txt) ]
		then
			# Save index of last line we read.
			latest="$i";
			break
		fi
	done

	# Get line info for new lines.
	for (( i=$latest; i >= 0; i-- ))
	do
		message_text=$(jq ".[$i].content" basecamp-campfire-1.json --raw-output | xmlstarlet fo -R -D - 2> /dev/null | xmlstarlet ed -d '//@*' | sed 's/<?xml version="1.0"?>//; s/<bc-attachment><figure><img><figcaption>/@/g; s/<\/figcaption><\/img><\/figure>//g; s/<\/bc-attachment><\/bc-attachment>//g; s/  //g; s/ &#xA0;//' | tr -d '\n')
		message_author=$(jq '.[0].creator.name' basecamp-campfire-1.json --raw-output)

	done
}


campfire-get

